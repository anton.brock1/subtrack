//
//  SubHubApp.swift
//  SubHub
//
//  Created by ANTON DOBRYNIN on 22.03.2024.
//

import SwiftUI

@main
struct SubHubApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
